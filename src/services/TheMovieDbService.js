import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

export default {
  baseUrl: 'https://api.themoviedb.org/3',
  apiKey: '?api_key=609d9a54e7a2c29a11fdb2de1919a8ad',
  getPopularMovies () {
    return Vue.http.get(this.baseUrl + '/movie/popular' + this.apiKey + '&sort_by=popularity.desc')
  },
  getMovieDetail (id) {
    return Vue.http.get(this.baseUrl + '/movie/' + id + this.apiKey)
  }
}
