export const HttpMixin = {
  data () {
    return {
      movies: [],
      movie: {},
      imageUrl: 'https://image.tmdb.org/t/p/w342',
      baseUrl: 'https://api.themoviedb.org/3',
      apiKey: '?api_key=609d9a54e7a2c29a11fdb2de1919a8ad'
    }
  },
  created: function () {
    return this.$http.get(this.baseUrl + '/movie/popular' + this.apiKey +
        '&sort_by=popularity.desc')
      .then(
        response => {
          this.movies = response.body
          console.log(response)
        },
        error => {
          console.log(error)
        }
      )
  }
}
