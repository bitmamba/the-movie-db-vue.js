import Vue from 'vue'
import Router from 'vue-router'
import MovieDetail from '@/components/MovieDetail'
import PopularMovieList from '../components/PopularMovieList.vue'
import Mixin from '../components/Mixin.vue'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/home',
    name: 'PopularMovieList',
    component: PopularMovieList
  },
  {
    path: '/movie/:id', // :id is telling Vue that we are expecting an parameter.
    name: 'MovieDetail',
    component: MovieDetail
  },
  {
    path: '/mixin',
    name: 'Mixin',
    component: Mixin
  },
    // this is the catch all url
  {
    path: '*',
    redirect: {
      name: 'PopularMovieList'
    }
  }
  ]
})
